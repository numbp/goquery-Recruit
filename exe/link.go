package exe

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"

	"goquery-foo.dev/models"

	"github.com/PuerkitoBio/goquery"
)

func GetHref() {
	var urls []string
	for i := 18; i < models.Pagelim; i++ {
		fileInfos, _ := ioutil.ReadFile(fmt.Sprintf("./db/link_%d.json", i))
		stringReader := strings.NewReader(string(fileInfos))
		page, err := goquery.NewDocumentFromReader(stringReader)
		if err != nil {
			log.Panic(err)
		}

		page.Find("body img.lazy").Each(func(_ int, s *goquery.Selection) {
			url, _ := s.Parent().Attr("href")
			url = fmt.Sprintf("%s%s", models.Siteurl, url)
			urls = append(urls, url)
		})
		page.Find("section > div.shopItem-body > a").Each(func(_ int, s *goquery.Selection) {
			url, _ := s.Attr("href")
			url = fmt.Sprintf("%s%s", models.Siteurl, url)
			urls = append(urls, url)
		})

	}

	u, err := json.Marshal(urls)
	if err != nil {
		log.Panic(err)
	}

	models.ArraySlice = len(string(u))
	fmt.Println(len(string(u)))

	_, err = os.Create("./db/link.json")
	ioutil.WriteFile("./db/link.json", []byte(u), 644)
}
