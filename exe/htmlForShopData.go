package exe

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"

	"goquery-foo.dev/models"

	"github.com/PuerkitoBio/goquery"
)

func GetDataForShopdata() {
	// HTMLのクセ
	var list []models.Data
	for i := 0; i < models.ArraySlice; i++ {
		fileInfos, _ := ioutil.ReadFile(fmt.Sprintf("./shops/%d.json", i))
		stringReader := strings.NewReader(string(fileInfos))
		page, err := goquery.NewDocumentFromReader(stringReader)
		if err != nil {
			log.Panic(err)
		}

		var ths1 []string
		page.Find("#shop-info > div > table > tbody > tr > th").Each(func(_ int, s *goquery.Selection) {
			ths1 = append(ths1, s.Text())
		})
		var tds1 []string
		page.Find("#shop-info > div > table > tbody > tr > td").Each(func(_ int, s *goquery.Selection) {
			tds1 = append(tds1, s.Text())
		})

		var ths2 []string
		page.Find("div > div.l-mainColumn > section > div > table > tbody > tr > th").Each(func(_ int, s *goquery.Selection) {
			ths2 = append(ths2, s.Text())
		})
		var tds2 []string
		page.Find("div > div.l-mainColumn > section > div > table > tbody > tr > td").Each(func(_ int, s *goquery.Selection) {
			tds2 = append(tds2, s.Text())
		})

		tel := page.Find("p.telNum").Text()
		if tel == "" {
			log.Println("エラー or tel中身がない")
		}
		email := page.Find("a.oubo_mail").Text()
		if email == "" {
			log.Println("エラー or email中身がない")
		}
		line := page.Find("div.line_id").Text()
		if line == "" {
			log.Println("エラー or line中身がない")
		}
		pr := page.Find("div.shopMessage-body > p").Text()
		if pr == "" {
			log.Println("エラー or pr中身がない")
		}
		var urls []string
		page.Find("div > table.shopInfo-tbl > tbody > tr > td > a").Each(func(_ int, s *goquery.Selection) {
			url, exists := s.Attr("href")
			if exists {
				urls = append(urls, url)
			}
		})

		d := new(models.Data)
		d.WhereID = models.Whare
		d.ID = int64(i)
		d.Name = tds1[0]
		d.Slug = ""
		// 配列がない場合の対処
		l := len(urls)
		// 配列が2つ以上ある場合
		// 末尾から2つ取り出す
		if 1 < l {
			d.Url = urls[l-2]
			d.UrlRecruit = urls[l-1]
		} else if l < 1 {
			d.Url = ""
			d.UrlRecruit = ""
		} else if l < 2 {
			d.Url = urls[l-1]
			d.UrlRecruit = ""
		}
		d.AddFirst = models.WhareJ
		d.AddSecond = ""
		d.AddThird = ""
		d.Address = tds1[1]
		d.TypeBiz = tds1[2]
		d.TypeJob = tds1[3]

		if len(tds1) < 8 {
			d.Access = "" // 交通の便アクセス
		} else {
			d.Access = tds1[7] // 交通の便アクセス
		}
		if len(tds1) < 9 {
			d.Fee = ""
		} else {
			d.Fee = tds1[8]
		}
		if len(tds1) < 10 {
			d.Require = ""
		} else {
			d.Require = tds1[9]
		}
		if len(tds1) < 11 {
			d.Treatment = ""
		} else {
			d.Treatment = tds1[10]
		}
		d.Time = ""
		d.Date = tds1[5]
		d.Tel = tel
		d.Line = line
		d.Email = email
		d.Descript = pr
		d.Other = ""

		list = append(list, *d)

		// if i%100 == 0 {
		// 	j, _ := json.Marshal(list)
		//
		// 	_, err := os.Create(fmt.Sprintf("./db/data_%d.json", i))
		// 	if err != nil {
		// 		log.Println("All jsonでエラー")
		// 	}
		// 	ioutil.WriteFile(fmt.Sprintf("./db/data_%d.json", i), []byte(j), 644)
		// }
	}

	j, _ := json.Marshal(list)

	_, err := os.Create("./db/data.json")
	if err != nil {
		log.Println("All jsonでエラー")
	}
	ioutil.WriteFile("./db/data.json", []byte(j), 644)

}
